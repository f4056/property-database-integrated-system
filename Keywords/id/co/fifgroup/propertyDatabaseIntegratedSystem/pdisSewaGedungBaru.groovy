package id.co.fifgroup.propertyDatabaseIntegratedSystem

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import id.co.fifgroup.propertyDatabaseIntegratedSystem.utilities
import java.awt.Robot
import java.awt.event.KeyEvent
import java.util.concurrent.ConcurrentHashMap.KeySetView
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.kms.katalon.core.configuration.RunConfiguration
import internal.GlobalVariable
import java.nio.file.Path
import java.nio.file.Paths
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import com.kms.katalon.core.webui.driver.DriverFactory


public class pdisSewaGedungBaru {

	@Keyword
	def inputDataRequetor(String jenisTransaksi, String jaringan, String jenisBarang, String jenisCabang, String vendor, String vendorSite){
		WebUI.click(findTestObject('Object Repository/2. Input Data Requestor/04_button_hamburgerMenu'))
		WebUI.click(findTestObject('Object Repository/2. Input Data Requestor/01_button_resposibility'))
		WebUI.click(findTestObject('Object Repository/2. Input Data Requestor/02_button_rent'))
		WebUI.click(findTestObject('Object Repository/2. Input Data Requestor/03_button_buatBaru'))
		WebUI.selectOptionByLabel(findTestObject('Object Repository/2. Input Data Requestor/05_select_jenisTransaksi'), jenisTransaksi, false)
		WebUI.click(findTestObject('Object Repository/2. Input Data Requestor/06_button_jaringan'))
		WebUI.setText(findTestObject('Object Repository/2. Input Data Requestor/07_inputText_jaringan'), jaringan)
		WebUI.click(findTestObject('Object Repository/2. Input Data Requestor/08_button_cari'))
		WebUI.click(findTestObject('Object Repository/2. Input Data Requestor/09_searchResult_jaringan'))
		WebUI.click(findTestObject('Object Repository/2. Input Data Requestor/10_button_jenisBarang'))
		WebUI.setText(findTestObject('Object Repository/2. Input Data Requestor/11_inputText_jenisBarang'), jenisBarang)
		WebUI.click(findTestObject('Object Repository/2. Input Data Requestor/12_button_cariJenisBarang'))
		WebUI.click(findTestObject('Object Repository/2. Input Data Requestor/13_searchResult_jenisBarang'))
		WebUI.selectOptionByLabel(findTestObject('Object Repository/2. Input Data Requestor/14_select_jenisCabang'), jenisCabang, false)
		WebUI.click(findTestObject('Object Repository/2. Input Data Requestor/15_button_vendor'))
		WebUI.setText(findTestObject('Object Repository/2. Input Data Requestor/16_inputText_vendor'), vendor)
		WebUI.click(findTestObject('Object Repository/2. Input Data Requestor/17_button_cariVendor'))
		WebUI.click(findTestObject('Object Repository/2. Input Data Requestor/18_searchResult_vendor'))
		WebUI.click(findTestObject('Object Repository/2. Input Data Requestor/19_button_vendorSite'))
		WebUI.setText(findTestObject('Object Repository/2. Input Data Requestor/20_inputText_vendorSite'), vendorSite)
		WebUI.click(findTestObject('Object Repository/2. Input Data Requestor/22_button_cariVendorSite'))
		WebUI.click(findTestObject('Object Repository/2. Input Data Requestor/21_searchResult_vendorSite'))
	}

	@Keyword
	def inputDataBagianDataGedungBaru(String informasiBangunan, String alamatBangunanBaru, String namaPemilikBangunan, String nomorTeleponPemilikBangunan, String jenisBangunan,
			String sumberAir, String kapasitasBebanListrik, String saluranTeleponYangTersedia, String unitPenarikanRatarata, String pajakPenghasilan, String hargaSewaPerTahun, String tanggalAwalSewa) {
		WebUI.click(findTestObject('Object Repository/3. Input Data Bagian Data Gedung Baru/01_var_button_informasiBangunan', [('informasiBangunan') : informasiBangunan]))
		WebUI.setText(findTestObject('Object Repository/3. Input Data Bagian Data Gedung Baru/02_inputText_alamatBangunanBaru'), alamatBangunanBaru)
		WebUI.setText(findTestObject('Object Repository/3. Input Data Bagian Data Gedung Baru/03_inputText_namaPemilikBangunan'), namaPemilikBangunan)
		WebUI.setText(findTestObject('Object Repository/3. Input Data Bagian Data Gedung Baru/04_inputText_nomorTeleponPemilikBangunan'), nomorTeleponPemilikBangunan)
		WebUI.click(findTestObject('Object Repository/3. Input Data Bagian Data Gedung Baru/05_button_jenisBangunan'))
		WebUI.setText(findTestObject('Object Repository/3. Input Data Bagian Data Gedung Baru/06_inputText_jenisBangunan'), jenisBangunan)
		WebUI.click(findTestObject('Object Repository/3. Input Data Bagian Data Gedung Baru/07_button_cariJenisBangunan'))
		WebUI.click(findTestObject('Object Repository/3. Input Data Bagian Data Gedung Baru/08_searchResult_jenisBangunan'))
		WebUI.setText(findTestObject('Object Repository/3. Input Data Bagian Data Gedung Baru/09_inputText_kapasitasBebanListrik'), kapasitasBebanListrik)
		WebUI.selectOptionByLabel(findTestObject('Object Repository/3. Input Data Bagian Data Gedung Baru/17_select_sumberAir'), sumberAir, false)
		WebUI.setText(findTestObject('Object Repository/3. Input Data Bagian Data Gedung Baru/10_inputText_saluranTeleponYangTersedia'), saluranTeleponYangTersedia)
		WebUI.click(findTestObject('Object Repository/3. Input Data Bagian Data Gedung Baru/11_checkBox_privateAutomaticBranchExchange(pabx)'))
		WebUI.setText(findTestObject('Object Repository/3. Input Data Bagian Data Gedung Baru/12_inputText_unitPenarikanRatarata'), unitPenarikanRatarata)
		WebUI.click(findTestObject('Object Repository/3. Input Data Bagian Data Gedung Baru/13_checkBox_pajakPertambahanNilai'))
		WebUI.selectOptionByLabel(findTestObject('Object Repository/3. Input Data Bagian Data Gedung Baru/14_select_pajakPenghasilan'), pajakPenghasilan, false)
		WebUI.setText(findTestObject('Object Repository/3. Input Data Bagian Data Gedung Baru/15_inputText_hargaSewaPerTahun'), hargaSewaPerTahun)
		WebUI.setText(findTestObject('Object Repository/3. Input Data Bagian Data Gedung Baru/16_inputText_tanggalAwalSewa'), tanggalAwalSewa)
	}

	@Keyword
	def uploadFotoGedungBaru(String jenisFoto1, String jenisFoto2, String jenisFoto3, jenisFoto4, String fotoBangunanBaru) {

		//sets KatalonDataFolder
		def projectDir = RunConfiguration.getProjectDir()
		Path projectPath = Paths.get(projectDir)
		Path imageFile1 = projectPath.resolve('Data Files').resolve(fotoBangunanBaru)
		String jpgFile = imageFile1

		println("projectPath: " + projectPath)
		println "imageFile1: " + imageFile1.toString()

		for (def index : (0..3)) {
			WebUI.scrollToElement(findTestObject('Object Repository/4. Upload Foto Gedung Baru/01_button_tambahkanFoto'), 2)
			WebUI.click(findTestObject('Object Repository/4. Upload Foto Gedung Baru/01_button_tambahkanFoto'))
		}
		WebUI.click(findTestObject('Object Repository/4. Upload Foto Gedung Baru/02_button_jenisFoto'))
		WebUI.click(findTestObject('Object Repository/4. Upload Foto Gedung Baru/03_select_jenisFoto', [('jenisFoto') : jenisFoto1]))
		utilities.uploadFile(findTestObject('Object Repository/4. Upload Foto Gedung Baru/04_button_upload'), jpgFile)

		WebUI.click(findTestObject('Object Repository/4. Upload Foto Gedung Baru/05_button_jenisFoto2'))
		WebUI.click(findTestObject('Object Repository/4. Upload Foto Gedung Baru/03_select_jenisFoto', [('jenisFoto') : jenisFoto2]))
		utilities.uploadFile(findTestObject('Object Repository/4. Upload Foto Gedung Baru/06_ button_upload2'), jpgFile)

		WebUI.click(findTestObject('Object Repository/4. Upload Foto Gedung Baru/09_button_jenisFoto3'))
		WebUI.click(findTestObject('Object Repository/4. Upload Foto Gedung Baru/03_select_jenisFoto', [('jenisFoto') : jenisFoto3]))
		utilities.uploadFile(findTestObject('Object Repository/4. Upload Foto Gedung Baru/11_button_upload3'), jpgFile)

		WebUI.click(findTestObject('Object Repository/4. Upload Foto Gedung Baru/10_button_jenisFoto4'))
		WebUI.click(findTestObject('Object Repository/4. Upload Foto Gedung Baru/03_select_jenisFoto', [('jenisFoto') : jenisFoto4]))
		utilities.uploadFile(findTestObject('Object Repository/4. Upload Foto Gedung Baru/12_button_upload4'), jpgFile)



	}

	@Keyword
	def deleteFotoGedung() {
		WebUI.click(findTestObject('Object Repository/4. Upload Foto Gedung Baru/07_checkBox_deleteFoto'))
		WebUI.click(findTestObject('Object Repository/4. Upload Foto Gedung Baru/08_button_hapusFotoTerpilih'))
	}

	@Keyword
	def inputLuasGedungBaru(String namaAtribut, String panjang, String lebar) {
		WebUI.click(findTestObject('Object Repository/5. Input Luas Gedung Baru/01_button_tambahkanAtribut'))
		WebUI.click(findTestObject('Object Repository/5. Input Luas Gedung Baru/02_button_namaAtribut'))
		WebUI.click(findTestObject('Object Repository/5. Input Luas Gedung Baru/03_select_jenisAttribut', [('jenisAttribut') : namaAtribut]))
		WebUI.setText(findTestObject('Object Repository/5. Input Luas Gedung Baru/04_inputText_panjang'), panjang)
		WebUI.setText(findTestObject('Object Repository/5. Input Luas Gedung Baru/05_inputText_lebar'), lebar)

		WebUI.click(findTestObject('Object Repository/5. Input Luas Gedung Baru/01_button_tambahkanAtribut'))
		String totalUkuranLuas = WebUI.getAttribute(findTestObject('Object Repository/5. Input Luas Gedung Baru/06_text_totalUkuranLuas'), 'value')
		KeywordUtil.logInfo('Total Ukuran Luas Gedung Baru ' +totalUkuranLuas)
		WebUI.click(findTestObject('Object Repository/5. Input Luas Gedung Baru/09_button_namaAtribut2'))
		WebUI.click(findTestObject('Object Repository/5. Input Luas Gedung Baru/03_select_jenisAttribut', [('jenisAttribut') : namaAtribut]))
		WebUI.setText(findTestObject('Object Repository/5. Input Luas Gedung Baru/10_inputText_panjang2'), panjang)
		WebUI.setText(findTestObject('Object Repository/5. Input Luas Gedung Baru/11_inputText_lebar2'), lebar)

		WebUI.click(findTestObject('Object Repository/5. Input Luas Gedung Baru/07_checkBox_atributBangunan'))
		WebUI.click(findTestObject('Object Repository/5. Input Luas Gedung Baru/08_button_hapusAtributTerpilih'))
	}

	@Keyword
	def inputDataBagianAlternatif1(String alamatBangunanAlternatif1, String namaPemilikBangunanAlternatif1, String nomorTeleponPemilikBangunan1,
			String jenisBangunan1, String kapasitasBebanListrik1, String saluranTeleponYangTersedia1, String sumberAir, String hargaTotal, String hargaBeliGedung,
			String hargaSewaGedungSekitar, String hargaBeliGedungSekitar) {

		WebUI.setText(findTestObject('Object Repository/6. Input Data Bagian Alternatif 1/01_input_alamatBangunan'), alamatBangunanAlternatif1)
		WebUI.setText(findTestObject('Object Repository/6. Input Data Bagian Alternatif 1/03_inputText_namaPemilikBangunan'), namaPemilikBangunanAlternatif1)
		WebUI.setText(findTestObject('Object Repository/6. Input Data Bagian Alternatif 1/04_inputText_nomorTeleponPemilikBangunan'), nomorTeleponPemilikBangunan1)
		WebUI.click(findTestObject('Object Repository/6. Input Data Bagian Alternatif 1/05_button_jenisBangunan'))
		WebUI.setText(findTestObject('Object Repository/6. Input Data Bagian Alternatif 1/06_inputText_jenisBangunan'), jenisBangunan1)
		WebUI.click(findTestObject('Object Repository/6. Input Data Bagian Alternatif 1/07_button_cariJenisBangunan'))
		WebUI.click(findTestObject('Object Repository/6. Input Data Bagian Alternatif 1/08_searchResult_jenisBangunan'))
		WebUI.setText(findTestObject('Object Repository/6. Input Data Bagian Alternatif 1/09_inputText_kapasitasBebanListrik'), kapasitasBebanListrik1)
		WebUI.setText(findTestObject('Object Repository/6. Input Data Bagian Alternatif 1/10_inputText_saluranTeleponYangTersedia'), saluranTeleponYangTersedia1)
		WebUI.click(findTestObject('Object Repository/6. Input Data Bagian Alternatif 1/11_checkBox_privateAutomaticBranchExchange(pabx)'))
		WebUI.click(findTestObject('Object Repository/6. Input Data Bagian Alternatif 1/13_checkBox_pajakPertambahanNilai'))
		WebUI.selectOptionByLabel(findTestObject('Object Repository/6. Input Data Bagian Alternatif 1/17_select_sumberAir'), sumberAir, false)
		WebUI.setText(findTestObject('Object Repository/6. Input Data Bagian Alternatif 1/18_inputText_hargaTotal'), hargaTotal)
		WebUI.setText(findTestObject('Object Repository/6. Input Data Bagian Alternatif 1/19_inputText_hargaBeliGedung(rp)'), hargaBeliGedung)
		WebUI.setText(findTestObject('Object Repository/6. Input Data Bagian Alternatif 1/20_inputText_hargaSewaGedungSekitar(rp)'), hargaSewaGedungSekitar)
		WebUI.setText(findTestObject('Object Repository/6. Input Data Bagian Alternatif 1/21_inputText_hargaBeliGedungSekitar(rp)'), hargaBeliGedungSekitar)
	}

	@Keyword
	def uploadFotoAlternatif1(String jenisFotoAlternatif1, String jenisFotoAlternatif2, String jenisFotoAlternatif3, String jenisFotoAlternatif4, fotoBangunanBaruAlternatif1) {

		//sets KatalonDataFolder
		def projectDir = RunConfiguration.getProjectDir()
		Path projectPath = Paths.get(projectDir)
		Path imageFile1 = projectPath.resolve('Data Files').resolve(fotoBangunanBaruAlternatif1)
		String jpgFile = imageFile1

		println("projectPath: " + projectPath)
		println "imageFile1: " + imageFile1.toString()

		for (def index : (0..3)) {
			WebUI.scrollToElement(findTestObject('Object Repository/7. Upload Foto Gedung Alternatif 1/01_button_tambahkanFoto'), 2)
			WebUI.click(findTestObject('Object Repository/7. Upload Foto Gedung Alternatif 1/01_button_tambahkanFoto'))
		}

		WebUI.click(findTestObject('Object Repository/7. Upload Foto Gedung Alternatif 1/02_button_jenisFoto'))
		WebUI.click(findTestObject('Object Repository/7. Upload Foto Gedung Alternatif 1/03_select_jenisFoto', [('jenisFoto') : jenisFotoAlternatif1]))
		utilities.uploadFile(findTestObject('Object Repository/7. Upload Foto Gedung Alternatif 1/04_button_upload'), jpgFile)

		WebUI.click(findTestObject('Object Repository/7. Upload Foto Gedung Alternatif 1/05_button_jenisFoto2'))
		WebUI.click(findTestObject('Object Repository/7. Upload Foto Gedung Alternatif 1/03_select_jenisFoto', [('jenisFoto') : jenisFotoAlternatif2]))
		utilities.uploadFile(findTestObject('Object Repository/7. Upload Foto Gedung Alternatif 1/06_ button_upload2'), jpgFile)

		WebUI.click(findTestObject('Object Repository/7. Upload Foto Gedung Alternatif 1/09_button_jenisFoto3'))
		WebUI.click(findTestObject('Object Repository/7. Upload Foto Gedung Alternatif 1/03_select_jenisFoto', [('jenisFoto') : jenisFotoAlternatif3]))
		utilities.uploadFile(findTestObject('Object Repository/7. Upload Foto Gedung Alternatif 1/11_button_upload3'), jpgFile)

		WebUI.click(findTestObject('Object Repository/7. Upload Foto Gedung Alternatif 1/10_button_jenisFoto4'))
		WebUI.click(findTestObject('Object Repository/7. Upload Foto Gedung Alternatif 1/03_select_jenisFoto', [('jenisFoto') : jenisFotoAlternatif4]))
		utilities.uploadFile(findTestObject('Object Repository/7. Upload Foto Gedung Alternatif 1/12_button_upload4'), jpgFile)
	}

	@Keyword
	def deleteFotoGedungAlternatif1() {
		WebUI.click(findTestObject('Object Repository/7. Upload Foto Gedung Alternatif 1/07_checkBox_deleteFoto'))
		WebUI.click(findTestObject('Object Repository/7. Upload Foto Gedung Alternatif 1/08_button_hapusFotoTerpilih'))
	}

	@Keyword
	def inputLuasAlternatif1(String namaAtribut, String panjang, String lebar) {
		WebUI.click(findTestObject('Object Repository/10. Input Luas Gedung Alternatif 1/01_button_tambahkanAtribut'))
		WebUI.click(findTestObject('Object Repository/10. Input Luas Gedung Alternatif 1/02_button_namaAtribut'))
		WebUI.click(findTestObject('Object Repository/10. Input Luas Gedung Alternatif 1/03_select_jenisAttribut', [('jenisAttribut') : namaAtribut]))
		WebUI.setText(findTestObject('Object Repository/10. Input Luas Gedung Alternatif 1/04_inputText_panjang'), panjang)
		WebUI.setText(findTestObject('Object Repository/10. Input Luas Gedung Alternatif 1/05_inputText_lebar'), lebar)

		WebUI.click(findTestObject('Object Repository/10. Input Luas Gedung Alternatif 1/01_button_tambahkanAtribut'))
		String totalUkuranLuas = WebUI.getAttribute(findTestObject('Object Repository/10. Input Luas Gedung Alternatif 1/06_text_totalUkuranLuas'), 'value')
		KeywordUtil.logInfo('Total Ukuran Luas Gedung Baru ' +totalUkuranLuas)
		WebUI.click(findTestObject('Object Repository/10. Input Luas Gedung Alternatif 1/09_button_namaAtribut2'))
		WebUI.click(findTestObject('Object Repository/5. Input Luas Gedung Baru/03_select_jenisAttribut', [('jenisAttribut') : namaAtribut]))
		WebUI.setText(findTestObject('Object Repository/10. Input Luas Gedung Alternatif 1/10_inputText_panjang2'), panjang)
		WebUI.setText(findTestObject('Object Repository/10. Input Luas Gedung Alternatif 1/11_inputText_lebar2'), lebar)

		WebUI.click(findTestObject('Object Repository/10. Input Luas Gedung Alternatif 1/07_checkBox_atributBangunan'))
		WebUI.click(findTestObject('Object Repository/10. Input Luas Gedung Alternatif 1/08_button_hapusAtributTerpilih'))
	}

	@Keyword
	def inputDataBagianAlternatif2(String alamatBangunanAlternatif2, String namaPemilikBangunanAlternatif2, String nomorTeleponPemilikBangunan2,
			String jenisBangunan2, String kapasitasBebanListrik2, String saluranTeleponYangTersedia2, String sumberAir, String hargaTotal, String hargaBeliGedung,
			String hargaSewaGedungSekitar, String hargaBeliGedungSekitar) {

		WebUI.setText(findTestObject('Object Repository/8. Input Data Bagian Alternatif 2/01_input_alamatBangunan'), alamatBangunanAlternatif2)
		WebUI.setText(findTestObject('Object Repository/8. Input Data Bagian Alternatif 2/03_inputText_namaPemilikBangunan'), namaPemilikBangunanAlternatif2)
		WebUI.setText(findTestObject('Object Repository/8. Input Data Bagian Alternatif 2/04_inputText_nomorTeleponPemilikBangunan'), nomorTeleponPemilikBangunan2)
		WebUI.click(findTestObject('Object Repository/8. Input Data Bagian Alternatif 2/05_button_jenisBangunan'))
		WebUI.setText(findTestObject('Object Repository/8. Input Data Bagian Alternatif 2/06_inputText_jenisBangunan'), jenisBangunan2)
		WebUI.click(findTestObject('Object Repository/8. Input Data Bagian Alternatif 2/07_button_cariJenisBangunan'))
		WebUI.click(findTestObject('Object Repository/8. Input Data Bagian Alternatif 2/08_searchResult_jenisBangunan'))
		WebUI.setText(findTestObject('Object Repository/8. Input Data Bagian Alternatif 2/09_inputText_kapasitasBebanListrik'), kapasitasBebanListrik2)
		WebUI.setText(findTestObject('Object Repository/8. Input Data Bagian Alternatif 2/10_inputText_saluranTeleponYangTersedia'), saluranTeleponYangTersedia2)
		WebUI.click(findTestObject('Object Repository/8. Input Data Bagian Alternatif 2/11_checkBox_privateAutomaticBranchExchange(pabx)'))
		WebUI.click(findTestObject('Object Repository/8. Input Data Bagian Alternatif 2/13_checkBox_pajakPertambahanNilai'))
		WebUI.selectOptionByLabel(findTestObject('Object Repository/8. Input Data Bagian Alternatif 2/17_select_sumberAir'), sumberAir, false)
		WebUI.setText(findTestObject('Object Repository/8. Input Data Bagian Alternatif 2/18_inputText_hargaTotal'), hargaTotal)
		WebUI.setText(findTestObject('Object Repository/8. Input Data Bagian Alternatif 2/19_inputText_hargaBeliGedung(rp)'), hargaBeliGedung)
		WebUI.setText(findTestObject('Object Repository/8. Input Data Bagian Alternatif 2/20_inputText_hargaSewaGedungSekitar(rp)'), hargaSewaGedungSekitar)
		WebUI.setText(findTestObject('Object Repository/8. Input Data Bagian Alternatif 2/21_inputText_hargaBeliGedungSekitar(rp)'), hargaBeliGedungSekitar)
	}

	@Keyword
	def uploadFotoAlternatif2(String jenisFotoAlternatif4, String jenisFotoAlternatif5, String jenisFotoAlternatif6, String jenisFotoAlternatif7, String fotoBangunanBaruAlternatif2) {

		//sets KatalonDataFolder
		def projectDir = RunConfiguration.getProjectDir()
		Path projectPath = Paths.get(projectDir)
		Path imageFile1 = projectPath.resolve('Data Files').resolve(fotoBangunanBaruAlternatif2)
		String jpgFile = imageFile1

		println("projectPath: " + projectPath)
		println "imageFile1: " + imageFile1.toString()

		for (def index : (0..3)) {
			WebUI.scrollToElement(findTestObject('Object Repository/9. Upload Foto Gedung Alternatif 2/01_button_tambahkanFoto'), 2)
			WebUI.click(findTestObject('Object Repository/9. Upload Foto Gedung Alternatif 2/01_button_tambahkanFoto'))
		}

		WebUI.click(findTestObject('Object Repository/9. Upload Foto Gedung Alternatif 2/02_button_jenisFoto'))
		WebUI.click(findTestObject('Object Repository/9. Upload Foto Gedung Alternatif 2/03_select_jenisFoto', [('jenisFoto') : jenisFotoAlternatif4]))
		utilities.uploadFile(findTestObject('Object Repository/9. Upload Foto Gedung Alternatif 2/04_button_upload'), jpgFile)

		WebUI.click(findTestObject('Object Repository/9. Upload Foto Gedung Alternatif 2/05_button_jenisFoto2'))
		WebUI.click(findTestObject('Object Repository/9. Upload Foto Gedung Alternatif 2/03_select_jenisFoto', [('jenisFoto') : jenisFotoAlternatif5]))
		utilities.uploadFile(findTestObject('Object Repository/9. Upload Foto Gedung Alternatif 2/06_ button_upload2'), jpgFile)

		WebUI.click(findTestObject('Object Repository/9. Upload Foto Gedung Alternatif 2/09_button_jenisFoto3'))
		WebUI.click(findTestObject('Object Repository/9. Upload Foto Gedung Alternatif 2/03_select_jenisFoto', [('jenisFoto') : jenisFotoAlternatif6]))
		utilities.uploadFile(findTestObject('Object Repository/9. Upload Foto Gedung Alternatif 2/11_button_upload3'), jpgFile)

		WebUI.click(findTestObject('Object Repository/9. Upload Foto Gedung Alternatif 2/10_button_jenisFoto4'))
		WebUI.click(findTestObject('Object Repository/9. Upload Foto Gedung Alternatif 2/03_select_jenisFoto', [('jenisFoto') : jenisFotoAlternatif7]))
		utilities.uploadFile(findTestObject('Object Repository/9. Upload Foto Gedung Alternatif 2/12_button_upload4'), jpgFile)

	}

	@Keyword
	def deleteFotoGedungAlternatif2() {
		WebUI.click(findTestObject('Object Repository/9. Upload Foto Gedung Alternatif 2/07_checkBox_deleteFoto'))
		WebUI.click(findTestObject('Object Repository/9. Upload Foto Gedung Alternatif 2/08_button_hapusFotoTerpilih'))
	}

	@Keyword
	def inputLuasAlternatif2(String namaAtribut, String panjang, String lebar) {
		WebUI.click(findTestObject('Object Repository/11. Input Luas Gedung Alternatif 2/01_button_tambahkanAtribut'))
		WebUI.click(findTestObject('Object Repository/11. Input Luas Gedung Alternatif 2/02_button_namaAtribut'))
		WebUI.click(findTestObject('Object Repository/5. Input Luas Gedung Baru/03_select_jenisAttribut', [('jenisAttribut') : namaAtribut]))
		WebUI.setText(findTestObject('Object Repository/11. Input Luas Gedung Alternatif 2/04_inputText_panjang'), panjang)
		WebUI.setText(findTestObject('Object Repository/11. Input Luas Gedung Alternatif 2/05_inputText_lebar'), lebar)

		WebUI.click(findTestObject('Object Repository/11. Input Luas Gedung Alternatif 2/01_button_tambahkanAtribut'))
		String totalUkuranLuas = WebUI.getAttribute(findTestObject('Object Repository/11. Input Luas Gedung Alternatif 2/06_text_totalUkuranLuas'), 'value')
		KeywordUtil.logInfo('Total Ukuran Luas Gedung Baru ' +totalUkuranLuas)
		WebUI.click(findTestObject('Object Repository/11. Input Luas Gedung Alternatif 2/09_button_namaAtribut2'))
		WebUI.click(findTestObject('Object Repository/5. Input Luas Gedung Baru/03_select_jenisAttribut', [('jenisAttribut') : namaAtribut]))
		WebUI.setText(findTestObject('Object Repository/11. Input Luas Gedung Alternatif 2/10_inputText_panjang2'), panjang)
		WebUI.setText(findTestObject('Object Repository/11. Input Luas Gedung Alternatif 2/11_inputText_lebar2'), lebar)

		WebUI.click(findTestObject('Object Repository/11. Input Luas Gedung Alternatif 2/07_checkBox_atributBangunan'))
		WebUI.click(findTestObject('Object Repository/11. Input Luas Gedung Alternatif 2/08_button_hapusAtributTerpilih'))
	}

	@Keyword
	def buildingDocuments(String uploadDocuments, String xlsDocument) {
		WebDriver driver = DriverFactory.getWebDriver()

		WebElement Table = driver.findElement(By.xpath("/html/body/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[2]/div/div/div[3]/div[2]/div[2]/div[8]/div[2]/div/div[1]/div/div[2]/table"))
		List<WebElement> rows_table = Table.findElements(By.tagName('tr'))
		println(rows_table)
		println(Table)

		int rows_count = rows_table.size()
		println(rows_count)
		for ( int x=0; x<9; x++) {
			utilities.uploadFile(findTestObject('Object Repository/12. Input Building Documenst Page/01_button_uploadDocuments',['addRow' : x]), uploadDocuments)
		}

		utilities.uploadFile(findTestObject('Object Repository/12. Input Building Documenst Page/02_button_uploadDocuments_xls'), xlsDocument)

		for ( int x=0; x<3; x++) {
			utilities.uploadFile(findTestObject('Object Repository/12. Input Building Documenst Page/03_button_uploaDocuments_pdf3',['addRow' : x]), uploadDocuments)
		}

	}

	@Keyword
	def buildingDocumentsAlternatif1(String uploadDocuments, String xlsDocument) {
		WebDriver driver = DriverFactory.getWebDriver()

		WebElement Table = driver.findElement(By.xpath("/html/body/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[2]/div/div/div[3]/div[2]/div[3]/div[8]/div[2]/div/div[1]/div/div[2]/table"))
		List<WebElement> rows_table = Table.findElements(By.tagName('tr'))
		println(rows_table)
		println(Table)

		int rows_count = rows_table.size()
		println(rows_count)
		for ( int x=0; x<9; x++) {
			utilities.uploadFile(findTestObject('Object Repository/12. Input Building Documenst Page/04_button_uploadDocAlternatif1',['addRow' : x]), uploadDocuments)
		}

		utilities.uploadFile(findTestObject('Object Repository/12. Input Building Documenst Page/05_button_uploadDocAlternatif1_xls'), xlsDocument)

		for ( int x=0; x<3; x++) {
			utilities.uploadFile(findTestObject('Object Repository/12. Input Building Documenst Page/06_button_uploadDocAlternatif1_pdf3',['addRow' : x]), uploadDocuments)
		}

	}

	@Keyword
	def buildingDocumentsAlternatif2(String uploadDocuments, String xlsDocument) {
		WebDriver driver = DriverFactory.getWebDriver()

		WebElement Table = driver.findElement(By.xpath("/html/body/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[2]/div/div/div[3]/div[2]/div[4]/div[8]/div[2]/div/div[1]/div/div[2]/table"))
		List<WebElement> rows_table = Table.findElements(By.tagName('tr'))
		println(rows_table)
		println(Table)

		int rows_count = rows_table.size()
		println(rows_count)
		for ( int x=0; x<9; x++) {
			utilities.uploadFile(findTestObject('Object Repository/12. Input Building Documenst Page/07_button_uploadDocAlternatif2',['addRow' : x]), uploadDocuments)
		}

		utilities.uploadFile(findTestObject('Object Repository/12. Input Building Documenst Page/08_button_uploadDocAlternatif2_xls'), xlsDocument)

		for ( int x=0; x<3; x++) {
			utilities.uploadFile(findTestObject('Object Repository/12. Input Building Documenst Page/09_button_uploadDocAlternatif2_pdf3',['addRow' : x]), uploadDocuments)
		}

	}
}

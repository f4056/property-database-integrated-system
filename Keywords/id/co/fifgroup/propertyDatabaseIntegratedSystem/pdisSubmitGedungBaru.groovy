package id.co.fifgroup.propertyDatabaseIntegratedSystem

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import java.awt.event.KeyEvent
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import org.openqa.selenium.Keys as Keys
import java.awt.Robot

import internal.GlobalVariable

public class pdisSubmitGedungBaru {
	@Keyword
	def menu(){
		WebUI.click(findTestObject('Object Repository/2. Input Data Requestor/04_button_hamburgerMenu'))
		WebUI.click(findTestObject('Object Repository/2. Input Data Requestor/01_button_resposibility'))
		WebUI.click(findTestObject('Object Repository/2. Input Data Requestor/02_button_rent'))
	}

	@Keyword
	def search(String nomorTrx){
		WebUI.setText(findTestObject('Object Repository/Submit Sewa Gedung/03 txtNoTransaksi'), nomorTrx)
		WebUI.click(findTestObject('Object Repository/Submit Sewa Gedung/02 btnCari'))
		WebUI.click(findTestObject('Object Repository/Submit Sewa Gedung/04 btnDetail'))
		WebUI.click(findTestObject('Object Repository/Submit Sewa Gedung/05 tabRIngkasan'))
		WebUI.scrollToElement(findTestObject('Object Repository/Submit Sewa Gedung/06 btnKirim'), 0)
		WebUI.click(findTestObject('Object Repository/Submit Sewa Gedung/06 btnKirim'))
		WebUI.waitForElementPresent(findTestObject('Object Repository/Submit Sewa Gedung/07 btnYa'), 4)
		WebUI.click(findTestObject('Object Repository/Submit Sewa Gedung/07 btnYa'))
		WebUI.delay(10)
		WebUI.click(findTestObject('Object Repository/Submit Sewa Gedung/22 btnCloseAlert'))
	}
	
	@Keyword
	def searchTrx(String nomorTrx) {
		WebUI.click(findTestObject('Object Repository/Submit Sewa Gedung/04 btnDetail'))
		WebUI.click(findTestObject('Object Repository/Submit Sewa Gedung/05 tabRIngkasan'))
		WebUI.scrollToElement(findTestObject('Object Repository/Submit Sewa Gedung/06 btnKirim'), 0)
		WebUI.click(findTestObject('Object Repository/Submit Sewa Gedung/06 btnKirim'))
		WebUI.waitForElementPresent(findTestObject('Object Repository/Submit Sewa Gedung/07 btnYa'), 4)
		WebUI.click(findTestObject('Object Repository/Submit Sewa Gedung/07 btnYa'))
		WebUI.delay(10)
		//WebUI.click(findTestObject('Object Repository/Submit Sewa Gedung/22 btnCloseAlert'))
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
//		WebUI.click(findTestObject('Object Repository/05_button_informasiOk'), FailureHandling.OPTIONAL)
//		WebUI.click(findTestObject('Object Repository/05_button_informasiOk'), FailureHandling.OPTIONAL)
	}

	@Keyword
	def approver(String nomorTrx, String remark){
		WebUI.setText(findTestObject('Object Repository/Submit Sewa Gedung/09 txtNoTrxApprover'), nomorTrx)
		WebUI.sendKeys(findTestObject('Object Repository/Submit Sewa Gedung/09 txtNoTrxApprover'), Keys.chord(Keys.ENTER))
		WebUI.click(findTestObject('Object Repository/Submit Sewa Gedung/10 dataFirst'))
		WebUI.scrollToElement(findTestObject('Object Repository/Submit Sewa Gedung/12 btnApprove'), 0)
		WebUI.setText(findTestObject('Object Repository/Submit Sewa Gedung/11 txtRemark'), remark)
		WebUI.click(findTestObject('Object Repository/Submit Sewa Gedung/12 btnApprove'))
		WebUI.click(findTestObject('Object Repository/Submit Sewa Gedung/13 btnYaApprove'))
		WebUI.scrollToElement(findTestObject('Object Repository/Logout/01_logout'), 0)
	}

	@Keyword
	def approverLast(String nomorTrx, String remark){
		WebUI.setText(findTestObject('Object Repository/Submit Sewa Gedung/09 txtNoTrxApprover'), nomorTrx)
		WebUI.sendKeys(findTestObject('Object Repository/Submit Sewa Gedung/09 txtNoTrxApprover'), Keys.chord(Keys.ENTER))
		WebUI.click(findTestObject('Object Repository/Submit Sewa Gedung/10 dataFirst'))
		WebUI.scrollToElement(findTestObject('Object Repository/Submit Sewa Gedung/12 btnApprove'), 0)
		WebUI.setText(findTestObject('Object Repository/Submit Sewa Gedung/11 txtRemark'), remark)
		WebUI.click(findTestObject('Object Repository/Submit Sewa Gedung/12 btnApprove'))
		WebUI.click(findTestObject('Object Repository/Submit Sewa Gedung/13 btnYaApprove'))
		WebUI.click(findTestObject('Object Repository/Submit Sewa Gedung/16 btnClose'))
		WebUI.click(findTestObject('Object Repository/Submit Sewa Gedung/17 btnOK1107'))
	}

	@Keyword
	def searchApproved(String nomorTrx){
		WebUI.setText(findTestObject('Object Repository/Submit Sewa Gedung/03 txtNoTransaksi'), nomorTrx)
		WebUI.click(findTestObject('Object Repository/Submit Sewa Gedung/02 btnCari'))
		WebUI.click(findTestObject('Object Repository/Submit Sewa Gedung/20 approve'))
		WebUI.scrollToElement(findTestObject('Object Repository/Submit Sewa Gedung/21 closeApprove'), 0)
		WebUI.delay(5)
		WebUI.click(findTestObject('Object Repository/Submit Sewa Gedung/21 closeApprove'))
	}
}

package id.co.fifgroup.propertyDatabaseIntegratedSystem

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import java.awt.Robot
import java.awt.event.KeyEvent
import java.util.concurrent.ConcurrentHashMap.KeySetView
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import org.openqa.selenium.Keys as Keys
import java.awt.Toolkit
import java.awt.datatransfer.StringSelection

import internal.GlobalVariable

public class utilities {

	@Keyword
	def loginWebPdis(String username, String password) {
		WebUI.setText(findTestObject('Object Repository/1. Login Page/01_inputText_username'), username)
		WebUI.setText(findTestObject('Object Repository/1. Login Page/02_inputText_password'), password)
		WebUI.click(findTestObject('Object Repository/1. Login Page/03_button_login'))
	}

	@Keyword
	def static uploadFile (TestObject to, String filePath) {
		WebUI.click(to)
		StringSelection ss = new StringSelection(filePath);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
		Robot robot = new Robot();
		//				robot.keyPress(KeyEvent.VK_ENTER);
		//				robot.keyRelease(KeyEvent.VK_ENTER);
		robot.delay(2000); //Millisecond 1 second delay only if needed
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.delay(2000); //Millisecond 1 second delay only if needed
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
	}

	@Keyword
	def takeScreenCapture(String fullname) {
		def date = new Date().format("HHmmss")


		WebUI.takeFullPageScreenshot(RunConfiguration.getProjectDir() + '/screenshoot/' +fullname+'screenshoot-'+date+'.jpg', FailureHandling.STOP_ON_FAILURE)
	}

	@Keyword
	def menuBackRent() {
		WebUI.click(findTestObject('Object Repository/2. Input Data Requestor/04_button_hamburgerMenu'))
		WebUI.click(findTestObject('Object Repository/2. Input Data Requestor/02_button_rent'))
	}

	@Keyword
	def rentInquiry(String transaksiAwal) {
		WebUI.setText(findTestObject('Object Repository/10_inputText_tanggalTransaksi'), transaksiAwal)
		WebUI.click(findTestObject('Object Repository/14. Rent Inquiry/07_button_cari'))
		WebUI.delay(3)
		WebUI.scrollToElement(findTestObject('Object Repository/13_button_terakhirDiubahOleh'), 2)
		WebUI.click(findTestObject('Object Repository/11_button_tanggalPerubahanTerakhir'))
		WebUI.delay(3)
		WebUI.click(findTestObject('Object Repository/11_button_tanggalPerubahanTerakhir'))
		WebUI.delay(3)
		String nomorTransaksi = WebUI.getText(findTestObject('Object Repository/14_text_nomorTransaksi'))
		GlobalVariable.NomorTransaksi = nomorTransaksi
		KeywordUtil.logInfo(GlobalVariable.NomorTransaksi)
		//WebUI.click(findTestObject('Object Repository/06_button_detail'))
	}

	@Keyword
	def logout() {
		WebUI.click(findTestObject('Object Repository/Logout/01_logout'))
	}
}

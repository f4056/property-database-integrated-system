import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

TestData excelDataSubmit = findTestData('Data Files/PDIS - Submit')
TestData excelData = findTestData('Data Files/PDIS - Data Files')

WebUI.openBrowser(GlobalVariable.baseUrl)

WebUI.maximizeWindow()

for (int i = 1; i <= excelDataSubmit.getRowNumbers(); i++) {
	
	//Login
	String username = excelDataSubmit.getValue('Username', i)
	String password = excelDataSubmit.getValue('Password', i)
	
	//Approve
	
	//String nomorTrx = GlobalVariable.NomorTransaksi.toString()
	//String nomorTrx = excelDataSubmit.getValue('Nomor Transaksi', i)
	String tanggal = excelData.getValue('Tanggal Awal Sewa', i)
	String Approve1 = excelDataSubmit.getValue('Approver 1', i)
	String Approve2 = excelDataSubmit.getValue('Approver 2', i)
	String Approve3 = excelDataSubmit.getValue('Approver 3', i)
	String Approve4 = excelDataSubmit.getValue('Approver 4', i)
	String Approve5 = excelDataSubmit.getValue('Approver 5', i)
	String Approve6 = excelDataSubmit.getValue('Approver 6', i)
	String Approve7 = excelDataSubmit.getValue('Approver 7', i)
	String remark = excelDataSubmit.getValue('Remarks', i)
	
	
	CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.utilities.loginWebPdis'(username, password)	
	CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.pdisSubmitGedungBaru.menu'()
	CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.utilities.rentInquiry'(tanggal)
	CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.pdisSubmitGedungBaru.searchTrx'(GlobalVariable.NomorTransaksi)
	//CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.pdisSubmitGedungBaru.search'(nomorTrx)
	CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.utilities.logout'()
	CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.utilities.loginWebPdis'(Approve1, password)
	CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.pdisSubmitGedungBaru.approver'(GlobalVariable.NomorTransaksi, remark)
	CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.utilities.logout'()
	CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.utilities.loginWebPdis'(Approve2, password)
	CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.pdisSubmitGedungBaru.approver'(GlobalVariable.NomorTransaksi, remark)
	CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.utilities.logout'()
	CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.utilities.loginWebPdis'(Approve3, password)
	CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.pdisSubmitGedungBaru.approver'(GlobalVariable.NomorTransaksi, remark)
	CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.utilities.logout'()
	CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.utilities.loginWebPdis'(Approve4, password)
	CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.pdisSubmitGedungBaru.approver'(GlobalVariable.NomorTransaksi, remark)
	CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.utilities.logout'()
	CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.utilities.loginWebPdis'(Approve5, password)
	CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.pdisSubmitGedungBaru.approver'(GlobalVariable.NomorTransaksi, remark)
	CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.utilities.logout'()
	CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.utilities.loginWebPdis'(Approve6, password)
	CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.pdisSubmitGedungBaru.approver'(GlobalVariable.NomorTransaksi, remark)
	CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.utilities.logout'()
	CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.utilities.loginWebPdis'(Approve7, password)
	CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.pdisSubmitGedungBaru.approverLast'(GlobalVariable.NomorTransaksi, remark)
	CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.utilities.logout'()
	CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.utilities.loginWebPdis'(username, password)
	CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.pdisSubmitGedungBaru.menu'()
	CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.pdisSubmitGedungBaru.searchApproved'(GlobalVariable.NomorTransaksi)
}

CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.utilities.logout'()

WebUI.closeBrowser()

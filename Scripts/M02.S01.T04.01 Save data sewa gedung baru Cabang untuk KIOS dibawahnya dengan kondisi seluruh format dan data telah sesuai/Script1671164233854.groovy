import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.awt.Robot
import java.awt.event.KeyEvent

TestData excelData = findTestData('Data Files/PDIS - Data Files')

WebUI.openBrowser(GlobalVariable.baseUrl)

WebUI.maximizeWindow()

for (int i = 1; i <= excelData.getRowNumbers(); i++) {
	//Login
	String username = excelData.getValue('Username', i)
	String password = excelData.getValue('Password', i)
	//Input Data Requestor
	String jenisTransaksi = excelData.getValue('Jenis Transaksi', i)
	String jaringan = excelData.getValue('Jaringan', i)
	String jenisBarang = excelData.getValue('Jenis Barang', i)
	String jenisCabang = excelData.getValue('Jenis Cabang', i)
	String vendor = excelData.getValue('Vendor', i)
	String vendorSite = excelData.getValue('Vendor Site', i)
	//Input Data Bagian Data Gedung Baru
	String informasiBangunan = excelData.getValue('Informasi Bangunan', i)
	String alamatBangunanBaru = excelData.getValue('Alamat Bangunan Baru', i)
	String namaPemilikBangunan = excelData.getValue('Nama Pemilik Bangunan', i)
	String nomorTeleponPemilikBangunan = excelData.getValue('Nomor Telepon Pemilik Bangunan', i)
	String sumberAir = excelData.getValue('Sumber Air', i)
	String jenisBangunan = excelData.getValue('Jenis Bangunan', i)
	String kapasitasBebanListrik = excelData.getValue('Kapasitas Beban Listrik', i)
	String saluranTeleponYangTersedia = excelData.getValue('Saluran Telepon Yang Tersedia', i)
	String unitPenarikanRatarata = excelData.getValue('Unit Penarikan Rata - Rata', i)
	String pajakPenghasilan = excelData.getValue('Pajak Penghasilan', i)
	String hargaSewaPerTahun = excelData.getValue('Harga Sewa Per Tahun', i)
	String tanggalAwalSewa = excelData.getValue('Tanggal Awal Sewa', i)
	//Upload Foto Gedung Baru
	String jenisFoto1 = excelData.getValue('Jenis Foto 1', i)
	String jenisFoto2 = excelData.getValue('Jenis Foto 2', i)
	String jenisFoto3 = excelData.getValue('Jenis Foto 3', i)
	String jenisFoto4 = excelData.getValue('Jenis Foto 4', i)
	String fotoBangunanBaru = excelData.getValue('Foto Bangunan Baru', i)
	//Input Luas Gedung Baru
	String namaAtribut = excelData.getValue('Nama Atribut Gedung Baru', i)
	String panjang = excelData.getValue('Panjang Gedung Baru', i)
	String lebar = excelData.getValue('Lebar Gedung Baru', i)
	//Input Data Bagian Alternatif 1
	String alamatBangunan1 = excelData.getValue('Alamat Bangunan Alternatif 1', i)
	String namaPemilikBangunan1 = excelData.getValue('Nama Pemilik Bangunan Alternatif 1', i)
	String nomorTeleponPemilikBangunan1 = excelData.getValue('Nomor Telepon Pemilik Bangunan Alternatif 1', i)
	String jenisBangunan1 = excelData.getValue('Jenis Bangunan Alternatif 1', i)
	String kapasitasBebanListrik1 = excelData.getValue('Kapasitas Beban Listrik Alternatif 1', i)
	String saluranTeleponYangTersedia1 = excelData.getValue('Saluran Telepon Yang Tersedia Alternatif 1', i)
	String hargaTotalAlternatif1 = excelData.getValue('Harga Total Alternatif 1', i)
	String hargaBeliGedungAlternatif1 = excelData.getValue('Harga Beli Gedung (Rp) Alternatif 1', i)
	String hargaSewaGedungSekitarAlternatif1 = excelData.getValue('Harga Sewa Gedung Sekitar (Rp) Alternatif 1', i)
	String sumberAir1 = excelData.getValue('Sumber Air Alternatif 1', i)
	String hargaBeliGedungSekitarAlternatif1 = excelData.getValue('Harga Beli Gedung Sekitar (Rp) Alternatif 1', i)
	//Input Luas Gedung Baru Alternatif 1
	String namaAtribut1 = excelData.getValue('Nama Atribut Gedung Baru Alternatif 1', i)
	String panjang1 = excelData.getValue('Panjang Gedung Baru Alternatif 1', i)
	String lebar1 = excelData.getValue('Lebar Gedung Baru Alternatif 1', i)
	//Input Data Bagian Alternatif 2
	String alamatBangunan2 = excelData.getValue('Alamat Bangunan Alternatif 2', i)
	String namaPemilikBangunan2 = excelData.getValue('Nama Pemilik Bangunan Alternatif 2', i)
	String nomorTeleponPemilikBangunan2 = excelData.getValue('Nomor Telepon Pemilik Bangunan Alternatif 2', i)
	String jenisBangunan2 = excelData.getValue('Jenis Bangunan Alternatif 2', i)
	String kapasitasBebanListrik2 = excelData.getValue('Kapasitas Beban Listrik Alternatif 2', i)
	String saluranTeleponYangTersedia2 = excelData.getValue('Saluran Telepon Yang Tersedia Alternatif 2', i)
	String hargaTotalAlternatif2 = excelData.getValue('Harga Total Alternatif 2', i)
	String hargaBeliGedungAlternatif2 = excelData.getValue('Harga Beli Gedung (Rp) Alternatif 2', i)
	String hargaSewaGedungSekitarAlternatif2 = excelData.getValue('Harga Sewa Gedung Sekitar (Rp) Alternatif 2', i)
	String sumberAir2 = excelData.getValue('Sumber Air Alternatif 2', i)
	String hargaBeliGedungSekitarAlternatif2 = excelData.getValue('Harga Beli Gedung Sekitar (Rp) Alternatif 2', i)
	//Input Luas Gedung Baru Alternatif 2
	String namaAtribut2 = excelData.getValue('Nama Atribut Gedung Baru Alternatif 2', i)
	String panjang2 = excelData.getValue('Panjang Gedung Baru Alternatif 2', i)
	String lebar2 = excelData.getValue('Lebar Gedung Baru Alternatif 2', i)
	//Document Bangunan
	String documentsBuilding = excelData.getValue('Document Bangunan', i)
	String xlsDocument = excelData.getValue('Booking 6 bulan terakhir untuk kios', i)
	//Rent Inquiry
	String tanggalAwalTransaksi = excelData.getValue('Tanggal Transaksi Awal', i)

CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.utilities.loginWebPdis'(username, password)

CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.pdisSewaGedungBaru.inputDataRequetor'(jenisTransaksi, jaringan, jenisBarang, jenisCabang, vendor, vendorSite)

CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.pdisSewaGedungBaru.inputDataBagianDataGedungBaru'(informasiBangunan, alamatBangunanBaru, namaPemilikBangunan, nomorTeleponPemilikBangunan, jenisBangunan, sumberAir, kapasitasBebanListrik,
	saluranTeleponYangTersedia, unitPenarikanRatarata, pajakPenghasilan, hargaSewaPerTahun, tanggalAwalSewa)

CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.pdisSewaGedungBaru.uploadFotoGedungBaru'(jenisFoto1, jenisFoto2, jenisFoto3, jenisFoto4, fotoBangunanBaru)

CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.pdisSewaGedungBaru.deleteFotoGedung'()

CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.pdisSewaGedungBaru.inputLuasGedungBaru'(namaAtribut, panjang, lebar)

CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.pdisSewaGedungBaru.buildingDocuments'(documentsBuilding, xlsDocument)

WebUI.click(findTestObject('Object Repository/00_button_selanjutnya'))

CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.pdisSewaGedungBaru.inputDataBagianAlternatif1'(alamatBangunan1, namaPemilikBangunan1, nomorTeleponPemilikBangunan1, jenisBangunan1, kapasitasBebanListrik1, saluranTeleponYangTersedia1, sumberAir1, hargaTotalAlternatif1, hargaBeliGedungAlternatif1, hargaSewaGedungSekitarAlternatif1, hargaBeliGedungSekitarAlternatif1)

CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.pdisSewaGedungBaru.uploadFotoAlternatif1'(jenisFoto1, jenisFoto2, jenisFoto3, jenisFoto4, fotoBangunanBaru)

CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.pdisSewaGedungBaru.deleteFotoGedungAlternatif1'()

CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.pdisSewaGedungBaru.inputLuasAlternatif1'(namaAtribut1, panjang1, lebar1)

CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.pdisSewaGedungBaru.buildingDocumentsAlternatif1'(documentsBuilding, xlsDocument)

WebUI.click(findTestObject('Object Repository/01_button_selanjutnyaAlternatif1'))

CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.pdisSewaGedungBaru.inputDataBagianAlternatif2'(alamatBangunan2, namaPemilikBangunan2, nomorTeleponPemilikBangunan2, jenisBangunan2, kapasitasBebanListrik2, saluranTeleponYangTersedia2, sumberAir2, hargaTotalAlternatif2, hargaBeliGedungAlternatif2, hargaSewaGedungSekitarAlternatif2, hargaBeliGedungSekitarAlternatif2)

CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.pdisSewaGedungBaru.uploadFotoAlternatif2'(jenisFoto1, jenisFoto2, jenisFoto3, jenisFoto4, fotoBangunanBaru)

CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.pdisSewaGedungBaru.deleteFotoGedungAlternatif2'()

CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.pdisSewaGedungBaru.inputLuasAlternatif2'(namaAtribut2, panjang2, lebar2)

CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.pdisSewaGedungBaru.buildingDocumentsAlternatif2'(documentsBuilding, xlsDocument)

WebUI.click(findTestObject('Object Repository/02_button_selanjutnyaRingkasan'))

CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.utilities.takeScreenCapture'('PDIS - ')

WebUI.click(findTestObject('Object Repository/08_button_submit'))

//WebUI.click(findTestObject('Object Repository/03_button_kirimUntukPersetujuan'))
//
WebUI.click(findTestObject('Object Repository/04_button_konfirmasiYa'))
//
WebUI.delay(5)
//

Robot robot = new Robot();
robot.keyPress(KeyEvent.VK_ENTER);
robot.keyRelease(KeyEvent.VK_ENTER);

//WebUI.click(findTestObject('Object Repository/05_button_informasiOk'))
////
//WebUI.click(findTestObject('Object Repository/05_button_informasiOk'), FailureHandling.OPTIONAL)


//WebUI.click(findTestObject('Object Repository/05_button_informasiOk'))

CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.utilities.menuBackRent'()
//
CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.utilities.rentInquiry'(tanggalAwalSewa)

WebUI.click(findTestObject('Object Repository/06_button_detail'))

CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.utilities.takeScreenCapture'('In Progress Ringkasan - ')

//
WebUI.click(findTestObject('Object Repository/07_button_ringkasanInquiry'))
//
CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.utilities.takeScreenCapture'('In Progress Ringkasan - ')

}

CustomKeywords.'id.co.fifgroup.propertyDatabaseIntegratedSystem.utilities.logout'()

WebUI.closeBrowser()
<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>03_select_jenisAttribut</name>
   <tag></tag>
   <elementGuidId>67c60561-4610-4607-be24-4aac84a8c100</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@class='z-combobox-popup z-combobox-open z-combobox-shadow']/ul/li/span[contains(text(),'${jenisAttribut}')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>

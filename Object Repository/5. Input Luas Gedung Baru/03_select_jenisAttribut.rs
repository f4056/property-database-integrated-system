<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>03_select_jenisAttribut</name>
   <tag></tag>
   <elementGuidId>a795fec3-6bad-47c6-9fe9-9c9e89a68cae</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@class='z-combobox-popup z-combobox-open z-combobox-shadow']/ul/li/span[contains(text(),'${jenisAttribut}')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>

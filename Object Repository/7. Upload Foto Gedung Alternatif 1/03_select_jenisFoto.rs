<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>03_select_jenisFoto</name>
   <tag></tag>
   <elementGuidId>a5d79bf8-a8eb-4fba-baf7-5b1985ff7d89</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@class='z-combobox-popup z-combobox-open z-combobox-shadow']/ul/li/span[contains(text(),'${jenisFoto}')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
